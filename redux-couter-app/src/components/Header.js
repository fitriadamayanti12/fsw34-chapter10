import { useDispatch, useSelector } from "react-redux";
import { authActions } from "../store/auth";
import classes from "./Header.module.css";

const Header = () => {
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.auth.isAuthenticated);

  const logoutHandler = () => {
    dispatch(authActions.logout());
  };

  return (
    <header className={classes.header}>
      <h1>Redux Auth</h1>
      {isAuth && (
        <nav>
          <ul>
            <li>
              <a href="/">My Tasks</a>
            </li>
          </ul>
          <li>
            <a href="/">My Favorites</a>
          </li>
          <li>
            <button onClick={logoutHandler}>Logout</button>
          </li>
        </nav>
      )}
    </header>
  );
};

export default Header;
