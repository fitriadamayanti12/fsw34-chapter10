import Head from "next/head";
import MeetupList from "@/components/meetups/MeetupList";
import { Fragment } from "react";
import { MongoClient } from "mongodb";

function HomePage(props) {
  return (
    <Fragment>
      <Head>
        <title>React Meetups</title>
        <meta
          name="description"
          content="Browse a huge list of highly active React meetups!"
        />
      </Head>
      <MeetupList meetups={props.meetups} />
    </Fragment>
  );
}

export async function getStaticProps() {
  // Fetch data from API
  const client = await MongoClient.connect(
    "mongodb+srv://damayantifitria1212:vvwWrGF9E2dnHyCS@cluster0.o0ovrek.mongodb.net/meetups?retryWrites=true&w=majority"
  );
  const db = client.db();

  const meetupsCollection = db.collection("meetups");

  const meetups = await meetupsCollection.find().toArray();

  client.close();

  return {
    props: {
      meetups: meetups.map((meetups) => ({
        title: meetups.title,
        address: meetups.address,
        image: meetups.image,
        id: meetups._id.toString(),
      })),
      revalidate: 1,
    },
  };
}

export default HomePage;
