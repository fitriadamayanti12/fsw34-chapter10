import { MongoClient } from "mongodb";

// POST -> /api/new-meetup

async function handler(req, res) {
  if (req.method === "POST") {
    const data = req.body;

    // Conncet to MongoDB Client
    const client = await MongoClient.connect(
      "mongodb+srv://damayantifitria1212:vvwWrGF9E2dnHyCS@cluster0.o0ovrek.mongodb.net/meetups?retryWrites=true&w=majority"
    );
    const db = client.db();

    const meetupCollection = db.collection("meetups");

    const result = await meetupCollection.insertOne(data);
    console.log(result);

    client.close();

    res.status(201).json({ message: "Meetup inserted!" });
  }
}

export default handler;
